$graph:
- baseCommand: stage-in
  class: CommandLineTool
  id: clt
  inputs:
    inp1:
      inputBinding:
        position: 1
        prefix: --target
      type: string
    inp2:
      inputBinding:
        position: 2
        prefix: --catalog
      type: File
  outputs:
    results:
      outputBinding:
        glob: .
      type: Any
  requirements:
    EnvVarRequirement:
      envDef:
        PATH: /opt/anaconda/envs/env_cwl/bin:/opt/anaconda/envs/env_s3/conda-otb/bin:/opt/anaconda/envs/env_repo2cli2/bin:/opt/anaconda/envs/env_hotspot/conda-otb/bin:/opt/anaconda/bin:/home/fbrito/.nvm/versions/node/v10.21.0/bin:/opt/anaconda/envs/notebook/bin:/opt/anaconda/envs/env_repo2cli/bin:/usr/share/java/maven/bin:/opt/anaconda/envs/notebook/bin:/opt/anaconda/bin:/usr/share/java/maven/bin:/opt/anaconda/bin:/opt/anaconda/condabin:/opt/anaconda/envs/notebook/bin:/opt/anaconda/bin:/usr/lib64/qt-3.3/bin:/usr/share/java/maven/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/fbrito/.local/bin:/home/fbrito/bin:/home/fbrito/.local/bin:/home/fbrito/bin
        PREFIX: /opt/anaconda/envs/env_nbr
    ResourceRequirement: {}
  stderr: std.err
  stdout: std.out
- class: Workflow
  doc: EO data stage-in
  id: stagein
  inputs:
    target:
      doc: Target folder for the stage-in
      label: Target folder for the stage-in
      type: string
    catalog:
      doc: catalog definition
      label: catalog definition
      type: File[]
  label: Normalized burn ratio
  outputs:
  - id: wf_outputs
    outputSource:
    - node_1/results
    type:
      items: Directory
      type: array
  requirements:
  - class: ScatterFeatureRequirement
  steps:
    node_1:
      in:
        inp1: target
        inp2: catalog
      out:
      - results
      run: '#clt'
      scatter: inp2
      scatterMethod: dotproduct
$namespaces:
  stac: http://www.me.net/stac/cwl/extension
cwlVersion: v1.0
