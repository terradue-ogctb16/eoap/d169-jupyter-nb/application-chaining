$graph:
- baseCommand: burned-area
  class: CommandLineTool
  id: clt
  inputs:
    inp1:
      inputBinding:
        position: 1
        prefix: --pre_event
      type: Directory
    inp2:
      inputBinding:
        position: 2
        prefix: --post_event
      type: Directory
    inp3:
      inputBinding:
        position: 3
        prefix: --ndvi_threshold
      type: string
    inp4:
      inputBinding:
        position: 4
        prefix: --ndwi_threshold
      type: string
    inp5:
      inputBinding:
        position: 5
        prefix: --aoi
      type: string
  outputs:
    results:
      outputBinding:
        glob: .
      type: Any
  requirements:
    EnvVarRequirement:
      envDef:
        PATH: /opt/anaconda/envs/env_burned_area/bin:/opt/anaconda/envs/env_s3/conda-otb/bin:/opt/anaconda/envs/env_repo2cli2/bin:/opt/anaconda/envs/env_hotspot/conda-otb/bin:/opt/anaconda/bin:/home/fbrito/.nvm/versions/node/v10.21.0/bin:/opt/anaconda/envs/notebook/bin:/opt/anaconda/envs/env_repo2cli/bin:/usr/share/java/maven/bin:/opt/anaconda/envs/notebook/bin:/opt/anaconda/bin:/usr/share/java/maven/bin:/opt/anaconda/bin:/opt/anaconda/condabin:/opt/anaconda/envs/notebook/bin:/opt/anaconda/bin:/usr/lib64/qt-3.3/bin:/usr/share/java/maven/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/fbrito/.local/bin:/home/fbrito/bin:/home/fbrito/.local/bin:/home/fbrito/bin
        PREFIX: /opt/anaconda/envs/env_nbr
    ResourceRequirement: {}
  stderr: std.err
  stdout: std.out
- class: Workflow
  doc: Burned area delineation using two techniques
  id: burned-area
  inputs:
    aoi:
      doc: Area of interest in WKT
      label: Area of interest
      type: string
    ndvi_threshold:
      doc: NDVI difference threshold
      label: NDVI difference threshold
      type: string
    ndwi_threshold:
      doc: NDWI difference threshold
      label: NDWI difference threshold
      type: string
    post_event:
      doc: Post-event product for burned area delineation
      label: Post-event product for burned area delineation
      type: Directory
    pre_event:
      doc: Pre-event product for burned area delineation
      label: Pre-event product for burned area delineation
      type: Directory
  label: Burned area delineation
  outputs:
  - id: wf_outputs
    outputSource:
    - node_1/results
    type:
      items: Directory
      type: array
  steps:
    node_1:
      in:
        inp1: pre_event
        inp2: post_event
        inp3: ndvi_threshold
        inp4: ndwi_threshold
        inp5: aoi
      out:
      - results
      run: '#clt'
cwlVersion: v1.0
