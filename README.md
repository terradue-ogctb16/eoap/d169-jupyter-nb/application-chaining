## Application chaining using CWL

### Worflow

**First step**

Stage-in the pre- and post event acquisitions with the `stage-in` CLI utility that takes as inputs:

- A YAML catalog definition file
- A target directory

And produces as output:

- a local STAC catalog with a single collection and items whose assets point to the local staged bands (e.g. `red`, `nir`, `swir16`, `swir22`)

**Second step**

The second step produces the vegetation indexes NDVI, NDWI and NBR. It takes as input the local STAC catalog.

This step produces as output:

- a local STAC catalog with a single item whose assets point to the 'NDVI', 'NDWI' and 'NBR' assets

The Jupyter Notebook is available at https://gitlab.com/terradue-ogctb16/eoap/d169-jupyter-nb/vegetation-index

**Third step**

This step takes as input a local STAC catalog with STAC catalog with a single item whose assets point to the 'NDVI', 'NDWI' and 'NBR' assets to produce a bitmask for the burned area based on the NDVI and NDWI thresholds and the Relativized Burn Ratio, an indicator of the burned area intensity .

The Jupyter Notebook is available at https://gitlab.com/terradue-ogctb16/eoap/d169-jupyter-nb/burned-area

### Chaining strategy

From TB16 CfP:

> Applications to be chained in this scenario should be a combination of Jupyter notebook based applications and Application Packages that link Docker Containers with arbitrary applications. 

The application chaining prepares the inputs and parameters for executing the three CWL scripts (available under folder `cwl`) and parses the outputs for the next step in the chaining.

The Jupyter Notebook is thus used to orchestrate the `cwltool` invoking the steps.

Applications to be chained in this scenario are a combination of Jupyter notebook based applications and Application Packages expressed as CWL.

While the execution with Docker containers follows exactily the same process and to ease the requirements on the runtime environment, the applications are not executed in Docker containers but locally. They were installed using a tool called `repo2cli` that takes a remote git repository, creates the conda environment and create a CLI utility to execute the notebook using the Jupyter Notebook APIs (nbconvert).

### Run the demonstrator using Binder 

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/terradue-ogctb16%2Feoap%2Fd169-jupyter-nb%2Fapplication-chaining/master?urlpath=lab)
